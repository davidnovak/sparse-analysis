#!/usr/bin/perl -w

use strict;
use Getopt::Long;

my ($lines,$objects,$inputcount,$remove_obj_file,$verbose,$noheadcnt) = (0,0,0,undef,0,0);
unless (GetOptions(
			"objects=i" => \$lines,
			"metaobs=i" => \$objects,
			"inputcount=i" => \$inputcount,
			"noheader-count=i" => \$noheadcnt,
			"remove:s" => \$remove_obj_file,
			"verbose+" => \$verbose
) && ($objects > 0 || $lines > 0)) {
	print STDERR "Usage: $0 [options] [files ...]\n";
	print STDERR "\t--objects <num>\tnumber of single-line objects to randomly select\n";
	print STDERR "\t--metaobs <num>\tnumber of metaobjects to randomly select\n";
	print STDERR "\t--inputcount <num>\tnumber of objects to expect as input (if not specified, counted using additional scan)\n";
	print STDERR "\t--noheader-count <num>\tindicates that there is no metaobject header and that each metaobject has <num> lines (comments excluded)\n";
	print STDERR "\t--remove\tthis flag says whether we _select_ or _remove_ random objects/lines from the file\n";
	print STDERR "\t\t\tan optional output file parameter can be supplied\n";
	print STDERR "\t--verbose\tshow information about selected objects\n";
	exit(1);
}
my $remove_objects = defined($remove_obj_file);
if ($remove_obj_file) {
	die "File '$remove_obj_file' already exists" if (-f $remove_obj_file);
	open(REMOVE_FILE, ">$remove_obj_file");
}

# Apply gzip decompression pipe to arguments
@ARGV = map { /\.(gz|Z)$/ ? "gzip -dc < $_ |" : $_ } @ARGV;

sub print_output_line {
	return if ($_[0] < 0);
	if ($_[0] ^ $remove_objects) {
		print $_[1];
	} elsif ($remove_obj_file) {
		print REMOVE_FILE $_[1];
	}
}

sub sanitize_header {
	# Sanitize object key for numbers
	return @_ unless ($_[1] =~ /^(#objectKey \S+)\s+(\d*)(.*)$/);
	return ($_[0], $2 ? "$1 $2\n" : "$1 $3\n");
}

sub print_header_output_line {
	return $_[0] if ($_[0] < 0);
	#print_output_line(sanitize_header(@_));
	print_output_line(@_);
	return $_[0];
}

# Method for dumping one metaobject
sub get_next_obj {
	my $sel_obj = $_[0];
	$_ = <> || return 0;
	# process the comments
	while (/^#/) {
		$sel_obj = print_header_output_line($sel_obj, $_);
		$_ = <> || return 0;
	}
	my $obj_desc_cnt;
	if ($noheadcnt > 0) {
		$obj_desc_cnt = $noheadcnt - 1;
	} else {
		my @descriptors = split(';');
		$obj_desc_cnt = int(scalar(@descriptors)/2);
		die "Wrong metaobject in input file $ARGV" if ($obj_desc_cnt <= 0);
	}
	do {
		print_output_line($sel_obj, $_);
	} while (($obj_desc_cnt-- > 0) && ($_ = <>));
	return 1;
}

sub get_next_line {
	my $sel_obj = $_[0];
	$_ = <> || return 0;
	# process the comments
	while (/^#/) {
		$sel_obj = print_header_output_line($sel_obj, $_);
		$_ = <> || return 0;
	}
	print_output_line($sel_obj, $_);
	return 1;
}

# Unify parameters
my $get_next = *get_next_line;
my $count = $lines;
if ($objects != 0) {
	$get_next = *get_next_obj;
	$count = $objects;
}

# Count lines if not specified
if ($inputcount == 0) {
	die "Input count must be specified for STDIN" unless (@ARGV);
	my @stored_files = @ARGV;
	while ($get_next->(-1)) {
		$inputcount++;
	}
	@ARGV = @stored_files;
	print STDERR "Counted $inputcount objects\n" if ($verbose);
}

die "Cannot select $count objects from $inputcount objects" if ($count >= $inputcount);

# Read objects and do intervals
my $curcnt = 0;
my $boundary = int($inputcount / $count);
my $bound_additional = $inputcount - $boundary*$count; # Rounding difference
$boundary++ if ($bound_additional-- > 0);
my $blockno = 0;
my $rand = int(rand($boundary));
print STDERR "Taking random object from intervals with $boundary (first $bound_additional intervals) and ", $boundary - 1, " (rest intervals) objects\n" if ($verbose);
do {
	if ($curcnt >= $boundary) {
		$rand = int(rand($boundary));
		$curcnt = 0;
		$blockno++;
		if ($bound_additional-- == 0) {
			$boundary--;
			$rand -- if ($rand == $boundary);
			print STDERR "Resizing interval to #$boundary objects\n" if ($verbose);
		}
		print STDERR "Selecting object #$rand from interval #$blockno\n" if ($verbose);
	}
} while ($get_next->($curcnt++ == $rand));

close(REMOVE_FILE) if ($remove_obj_file);
