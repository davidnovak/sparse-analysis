#!/usr/bin/python
from compiler.ast import obj

import numpy as np
import argparse
import sys
import shlex, subprocess
from subprocess import PIPE
import re

parser = argparse.ArgumentParser('runs a series of queries using a C++ client to a running C++ server')
parser.add_argument('-p', '--port', help='port of the C++ server', required=True)
parser.add_argument('-q', '--query', help='file with query lines', required=True)
parser.add_argument('-i', '--ids', help='file with obj-id mapping')
parser.add_argument('--query_ids', help='file with query-id mapping')
parser.add_argument('-k', help='number of nearest neighbors')
args = parser.parse_args()

k = 10
if args.k:
    k = int(args.k)
cmd = './query_server/query_client -p %i -a localhost -k %i' % (int(args.port), k)
line_matcher = re.compile('id=([0-9]*) dist=(.*)$')

# preprocessing of the ID paramaters
def load_id_file(file):
    """
    given a file with a list of IDs (one per line) this function creates a list of these ids
    :param file:
    :return: a list of string IDs
    """
    if not file:
        return None
    reader = open(file, 'r')
    ret_val = []
    for line in reader:
        ret_val.append(line.strip())
    return ret_val


query_ids = load_id_file(args.query_ids)
object_ids = load_id_file(args.ids)


def run_query(query_str):
    """

    :param query_str:
    :return:
    """
    print >> sys.stderr, "running command: " + str(cmd)
    #print >> sys.stderr, "parameter: " + query_str,
    gproc = subprocess.Popen(shlex.split(cmd), stdin=PIPE, stdout=PIPE)
    outdata, _ = gproc.communicate(input=query_str)

    return outdata


def get_query_str(queryidx):
    if query_ids:
        return query_ids[int(queryidx)]
    return str(queryidx)


def get_obj_id(objectidx):
    if object_ids:
        return object_ids[int(objectidx)]
    return str(objectidx)


def transform_dist(distance):
    return str(1.0 - float(distance))


# Running a 10-NN query
# Finished in: 14.807 ms
#id=65675 dist=0.444566
#id=441556 dist=0.489753
#id=434825 dist=0.512422
def process_output(queryidx, output):
    """
    process output from the NMSL k-NN query processing
    :param queryidx: index of the query (starting from 0)
    :param output: the output text
    :return: a string to be printed out formatted as Jeroen likes it  and running time
    """
    query_id_str = get_query_str(queryidx)
    running_time = None
    ret_val = ""
    for line in output.splitlines():
        if not line.startswith('id='):
            if line.startswith('Finished in: '):
                m = re.match('Finished in: ([0-9\.]*) .*', line)
                running_time = int(m.group(1))
            continue
        result = line_matcher.match(line)
        ret_val += query_id_str + '\t' + get_obj_id(result.group(1)) + '\t' + transform_dist(result.group(2)) + '\n'
    return ret_val, running_time


def main():
    reader = open(args.query, 'r')
    i = 0
    time_sum = 0
    for line in reader:
        print >> sys.stderr, 'query line number ' + str(i)
        output = run_query(line)
        result, running_time = process_output(i, output)
        print result
        print >> sys.stderr, 'running time (ms): ' + str(running_time)
        time_sum += running_time
        i += 1
    print >> sys.stderr, 'average runnning time (ms): ' + (float(time_sum) / i) if i > 0 else ''


main()
