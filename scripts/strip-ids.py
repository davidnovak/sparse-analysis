#!/usr/bin/python

import re
import argparse
import sys


parser = argparse.ArgumentParser('given file with data, it strips the first column with IDs from the data')
parser.add_argument("-d", "--datafile", help='file with space separated values', required=True)
parser.add_argument("-o", "--output", help='specify output file, default: <DATAFILE>.ids')
parser.add_argument("-i", "--id_file", help='specify file to write the list of ids into, default: <DATAFILE>.data')
args = parser.parse_args()

if not args.output:
    args.output = args.datafile + '.data'
if not args.id_file:
    args.id_file = args.datafile + '.ids'


def main():
    p = re.compile('^([^\s]*)\s(.*)')
    # crate the output text file
    reader = open(args.datafile, 'r')
    out = open(args.output, 'w')
    id_file = open(args.id_file, 'w')
    for line in reader:
        m = p.match(line)
        id_file.write(m.group(1) + '\n')
        out.write(m.group(2) + '\n')
    out.close()
    id_file.close()


main()

