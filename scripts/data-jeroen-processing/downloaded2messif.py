#!/usr/bin/python

import re
import urllib
import argparse
import sys
import os
import glob
import lz4tools
import tarfile

pattern = '*.tar.lz4'
#pattern = '*.tar'

parser = argparse.ArgumentParser('given a directory with downloaded lz4 files, this scripts creates a single file with all documents')
parser.add_argument("-d", "--directory", help='regexp pattern to search for in the file, default: ' + pattern, required=True)
parser.add_argument("-p", "--pattern", help='regexp pattern to search for in the dir, default: ' + pattern)
parser.add_argument("-o", "--output", help='specify output file', default='messif.data')
parser.add_argument("-i", "--id_file", help='specify file to write the list of ids into', default='messif.ids')
args = parser.parse_args()

if args.pattern:
    pattern = args.pattern


def process_text_file(reader, out):
    pairs = []
    for line in reader:
        pairs.append(line.strip().split('\t'))
        #out.write(line.strip())
        #out.write('\t')
    pairs.sort(key=lambda tup: int(tup[0]))
    out.write('\t'.join(x + '\t' + y for [x, y] in pairs))
    out.write('\n')


def process_tar_file(file_tar_lz4, out, id_file):
    print >> sys.stderr, "... processing " + file_tar_lz4
    archive_file = lz4tools.openTar(file_tar_lz4)
    for filename in archive_file.getmembers():
        if not filename.isfile():
            continue
        id_file.write(filename.name + '\n')
        text_file = archive_file.extractfile(filename)
        process_text_file(text_file, out)
    #print archive_file.list()


def main():
    # crate the output text file
    out = open(args.directory + '/' + args.output, 'w')
    id_file = open(args.directory + '/' + args.id_file, 'w')
    for file in glob.iglob(args.directory + '/' + pattern):
        process_tar_file(file, out, id_file)
    out.close()
    id_file.close()


main()
#lz4tools.compressFileDefault('source0.tar')
#archive_file = lz4tools.openTar('source0.tar.lz4')