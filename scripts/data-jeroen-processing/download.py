#!/usr/bin/python

import re
import urllib
import argparse
import sys

pattern = '[^"\/]*\.tar\.lz4'
url_prefix = 'http://beehub.nl/home/jeroenv/robust/source.tfidf/'
index_file = 'index.html'

parser = argparse.ArgumentParser('his script 1) downloads the index.html from the url; 2) parses the index.html for all mentions of given pattern and downloads given files.')
parser.add_argument("-u", "--url", help='URL with a list of files, e.g. '+ url_prefix, required=True)
parser.add_argument("-p", "--pattern", help='regexp pattern to search for in the file, default: ' + pattern, default=pattern)
args = parser.parse_args()


def download_index():
    print >> sys.stderr, 'Downloading ' + index_file
    urllib.urlretrieve(args.url, index_file)


def download(file):
    print >> sys.stderr, '... downloading ' + file
    urllib.urlretrieve(args.url+ file, file)


def main():
    """
    Given an URL, this script 1) downloads the index.html from the url; 2) parses the index.html for all
      mentions of given pattern and downloads given files.
    :return:
    """
    download_index()
    index = open(index_file)
    downloaded = set([])
    for line in index:
        search = re.search('"(' + args.pattern + ')"', line)
        if not search:
            continue
        file = search.group(1)
        if file and file not in downloaded:
            download(file)
            downloaded.add(file)


main()
