
import os
import sys
import os.path
import gzip
import re

if len(sys.argv) < 2:
    print "Usage:", sys.argv[0], "<messif_approx_file> [<output_file>]"
    print "\tparses output from MESSIF-driven kNN experiment; transfers the results into format for Jeroen evaluatio tools"
    print "\tTODO: finish this description"
    sys.exit(1)

# input file with data
if os.path.splitext(sys.argv[1])[1] == ".gz":
    datafile = gzip.open(sys.argv[1], "rb")
else:
    datafile = open(sys.argv[1], "r")

# outputfile
outstream = sys.stdout
if len(sys.argv) > 2:
    outstream = open(sys.argv[2], "w")

queryid = ""
p = re.compile('Approx.* <.* \(([0-9]*)\),')
line = datafile.readline()
while line != "" and line.strip() != "":
    if line.startswith("Previously"):
        pass
    elif line.startswith("ApproxKNNQueryOperation"):
        m = p.match(line)
        queryid = m.group(1)
    else:
        answers = re.split(', ', line.strip())
        for answer in answers:
            id_dist = re.split(': ', answer)
            #outstream.write(queryid + '\t ' + id_dist[0] + '\t ' +id_dist[1] + '\n')
	    outstream.write(queryid + '\t ' + id_dist[0] + '\t ' +str((float(1.0) - float(id_dist[1]))) + '\n')
    line = datafile.readline()


