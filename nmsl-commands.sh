
NEW SPARSE SPACE

VISUAL WORDS:

similarity_search/release/experiment --distType float --spaceType cosinesimil_sparse_fast --queryFile data/visual-words/data/tfidf-1m-queries1000.txt --knn 1,10 --dataFile data/visual-words/data/tfidf-1m-1m.txt --outFilePrefix results/visual-words/results.txt -g results/visual-words/gs_cache_1m-1m-q1000 --method napp -c numPivot=4096,numPivotIndex=100,pivotFile=data/visual-words/pivots-1m-d10000-n4096.txt -t numPivotSearch=10 -t numPivotSearch=15 -t numPivotSearch=20 -S results/visual-words/napp-tfidf-1m-1m-d10000-n4096.bin --appendToResFile 0



release/experiment -i ~/TextCollect/VectorSpaces/wikipedia.txt -s cosinesimil_sparse_fast -D 1000000 -b 1 -k 10 -g gs_cache/wikipedia_cosine_1M  -Q 2000 -m napp -c numPivot=2000,numPivotIndex=100,pivotFile=pivots/wikipedia_sparse_pivots10K_maxId_100K.txt -t numPivotSearch=10 -t numPivotSearch=11 -t numPivotSearch=12 -t numPivotSearch=13 -t numPivotSearch=14  -L napp_cosine_numPivot=2000_termQty10K 2>&1|tee log.cosine





PAN11:
similarity_search/release/experiment --distType float --spaceType cosinesimil_sparse_fast --queryFile pan11/tf-idf/suspicious/suspicious-nokeys.data --knn 1,10,50,100 --dataFile pan11/tf-idf/source/source-nokeys.data --outFilePrefix pan11/tf-idf/results/proj_incsort/proj_incsort --method proj_incsort:projType=perm,projDim=256,dbScanFrac=0.00902,pivotFile=pan11/tf-idf/pivots/pivots-random-256-noid.data --method proj_incsort:projType=perm,projDim=256,dbScanFrac=0.04508,pivotFile=pan11/tf-idf/pivots/pivots-random-256-noid.data  --method proj_incsort:projType=perm,projDim=256,dbScanFrac=0.0902,pivotFile=pan11/tf-idf/pivots/pivots-random-256-noid.data --cachePrefixGS pan11/tf-idf/results/cpp-ground-truth --appendToResFile 1


wiki-sparse - random pivots
similarity_search/release/experiment --distType float --spaceType cosinesimil_sparse_fast --queryFile wiki-sparse/queries-1000.txt --knn 1,10,50,100 --dataFile wiki-sparse/wiki-100K.txt --outFilePrefix wiki-sparse/results-100K/proj_incsort_perm --method proj_incsort:projType=perm,projDim=1024,dbScanFrac=0.01 --method proj_incsort:projType=perm,projDim=1024,dbScanFrac=0.05 --method proj_incsort:projType=perm,projDim=1024,dbScanFrac=0.1 --cachePrefixGS wiki-sparse/results-100K/ground-truth --appendToResFile 1

similarity_search/release/experiment --distType float --spaceType cosinesimil_sparse_fast --queryFile wiki-sparse/queries-1000.txt --knn 1,10,50,100 --dataFile wiki-sparse/wiki-100K.txt --outFilePrefix wiki-sparse/results-100K/proj_incsort_perm --method proj_incsort:projType=perm,projDim=1024,dbScanFrac=0.01,pivotFile=wiki-sparse/pivots-kmeans-256.txt --method proj_incsort:projType=perm,projDim=1024,dbScanFrac=0.05,pivotFile=wiki-sparse/pivots-kmeans-256.txt --method proj_incsort:projType=perm,projDim=1024,dbScanFrac=0.1,pivotFile=wiki-sparse/pivots-kmeans-256.txt --cachePrefixGS wiki-sparse/results-100K/ground-truth --appendToResFile 1
:q

wiki-sparse 400K:

incremental sort of full permutations


similarity_search/release/experiment --distType float --spaceType cosinesimil_sparse_fast --queryFile wiki-sparse/queries-1000-out.txt --knn 1,10 --dataFile wiki-sparse/400K/wiki-400K.txt --outFilePrefix wiki-sparse/results-400K/proj_incsort --cachePrefixGS wiki-sparse/results-400K/ground-truth --appendToResFile 1 --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.01 --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.05 --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.1 --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.01,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-512.txt --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.05,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-512.txt --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.1,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-512.txt --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.01,pivotFile=wiki-sparse/400K/pivots-unioned20-512.txt --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.05,pivotFile=wiki-sparse/400K/pivots-unioned20-512.txt --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.1,pivotFile=wiki-sparse/400K/pivots-unioned20-512.txt --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.01,pivotFile=wiki-sparse/400K/pivots-unioned20-512-filtered1.txt --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.05,pivotFile=wiki-sparse/400K/pivots-unioned20-512-filtered1.txt --method proj_incsort:projType=perm,projDim=512,dbScanFrac=0.1,pivotFile=wiki-sparse/400K/pivots-unioned20-512-filtered1.txt 

similarity_search/release/experiment --distType float --spaceType cosinesimil_sparse_fast --queryFile wiki-sparse/queries-1000-out.txt --knn 1,10 --dataFile wiki-sparse/400K/wiki-400K.txt --outFilePrefix wiki-sparse/results-400K/proj_incsort --cachePrefixGS wiki-sparse/results-400K/ground-truth --appendToResFile 1 --method proj_incsort:projType=perm,projDim=2048,dbScanFrac=0.051 --method proj_incsort:projType=perm,projDim=2048,dbScanFrac=0.072 --method proj_incsort:projType=perm,projDim=2048,dbScanFrac=0.113 --method proj_incsort:projType=perm,projDim=2048,dbScanFrac=0.15 --method proj_incsort:projType=perm,projDim=2048,dbScanFrac=0.001,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method proj_incsort:projType=perm,projDim=2048,dbScanFrac=0.002,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method proj_incsort:projType=perm,projDim=2048,dbScanFrac=0.017,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method proj_incsort:projType=perm,projDim=2048,dbScanFrac=0.054,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt



NAPP 2048 pivots:
similarity_search/release/experiment --distType float --spaceType cosinesimil_sparse_fast --queryFile wiki-sparse/queries-1000-out.txt --knn 1,10 --dataFile wiki-sparse/400K/wiki-400K.txt --outFilePrefix wiki-sparse/results-400K/napp --cachePrefixGS wiki-sparse/results-400K/ground-truth --appendToResFile 1 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=1,indexThreadQty=4,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=2,indexThreadQty=4,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=3,indexThreadQty=4,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=4,indexThreadQty=4,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=5,indexThreadQty=4,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=6,indexThreadQty=4,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=8,indexThreadQty=4,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=10,indexThreadQty=4,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=1,indexThreadQty=4,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=2,indexThreadQty=4,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=3,indexThreadQty=4,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=4,indexThreadQty=4,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=5,indexThreadQty=4,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=6,indexThreadQty=4,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=8,indexThreadQty=4,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=10,indexThreadQty=4,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt 




wiki sparse 4M:

similarity_search/release/experiment --distType float --spaceType cosinesimil_sparse_fast --queryFile wiki-sparse/queries-1000-out.txt --knn 1,10 --dataFile wiki-sparse/4M/wiki-4M.txt --outFilePrefix wiki-sparse/results-4M/napp --cachePrefixGS wiki-sparse/results-4M/ground-truth --appendToResFile 1 

--method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=1,indexThreadQty=10,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=2,indexThreadQty=10,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=3,indexThreadQty=10,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=4,indexThreadQty=10,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=5,indexThreadQty=10,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=6,indexThreadQty=10,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=7,indexThreadQty=10,useSort=0 --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=8,indexThreadQty=10,useSort=0 
--method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=1,indexThreadQty=10,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=2,indexThreadQty=10,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=3,indexThreadQty=10,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=4,indexThreadQty=10,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=5,indexThreadQty=10,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=6,indexThreadQty=10,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=7,indexThreadQty=10,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt --method napp:numPivot=2048,numPivotIndex=64,numPivotSearch=8,indexThreadQty=10,useSort=0,pivotFile=wiki-sparse/400K/pivots-dimproj5000-flat-2048.txt 



