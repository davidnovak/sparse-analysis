#!/usr/bin/python


import numpy as np
import argparse

parser = argparse.ArgumentParser('generate random projection pivots for sparse vectors')
parser.add_argument('-a', '--max_termid', help='maximum term id to be selected (inclusive)', type=int, required=True)
parser.add_argument('-i', '--min_termid', help='minimum term id to be selected (inclusive, default: 0)', type=int, default=0)
parser.add_argument('-d', '--dimensionality', help='number of random projections to generate', type=int, required=True)
parser.add_argument('-n', '--pivot_number', help='number of pivots to generate', type=int, required=True)
args = parser.parse_args()


def generate_random_projection_pivots(args):
    """
    Generates given number of our random projection pivots.
    :param args:
    :return:
    """
    value = np.sqrt(float(1) / float(args.dimensionality))
    for i in range(0, args.pivot_number):
        pivots = set()
        while len(pivots) < args.dimensionality:
            pivots.update(map(lambda x: x+args.min_termid, np.random.choice(args.max_termid - args.min_termid + 1, args.dimensionality - len(pivots))))

        sorted = list(pivots)
        sorted = np.sort(sorted)
        pivotstr = ''
        for d in sorted:
            pivotstr += str(d) + ':' + ('%.3f' % value) + ' '
        print pivotstr


generate_random_projection_pivots(args)
