#!/bin/bash


DATADIR=datasets/wiki-sparse/100K
#DATADIR=datasets/visual-words
DATAFILE=$DATADIR/wiki-100K.txt.gz
#DATAFILE=$DATADIR/tfidf-1m-1m.txt
LOGDIR=$DATADIR/logs
mkdir -p $LOGDIR


DATAFILESIZE=100000
PIVOTNUMBERS=1000
MAX_TERMID=100000
MIN_TERMID=1


function prepare_run {
    # first param: pivot file output (without .txt)
    # second param: comment text
    PIVOTFILE=$DATADIR/$1.txt
    LOGFILE=$LOGDIR/$1.log
    echo "$2"
    echo "        generating pivot file: $PIVOTFILE, see log file $LOGFILE"
}


for PIVOTNUMBER in $PIVOTNUMBERS; do
    echo "starting process of selecting $PIVOTNUMBER pivots"
    echo

    prepare_run pivots-random-${PIVOTNUMBER} "selecting objects in random"
    ./makerandom.pl --objects $PIVOTNUMBER --inputcount $DATAFILESIZE --verbose $DATAFILE > $PIVOTFILE 2>$LOGFILE
    echo

    # do not run this selection, if you run the following one
    #prepare_run pivots-kmeans-${PIVOTNUMBER} "running k-means clustering"
    #java -cp "jars/*" sparse.pivot.ChoosePivots -c messif.objects.impl.ObjectSparseVectorCos -d ${DATAFILE} -n ${PIVOTNUMBER} 2>$LOGFILE | grep -v "^#filter.*" > $PIVOTFILE
    #echo

    #PIVOTFILE2=$DATADIR/pivots-kmeans-${PIVOTNUMBER}.txt
    #prepare_run pivots-mergedkmeans32-${PIVOTNUMBER} "run k-means clustering, print the clustroids (into $PIVOTFILE2)
    #and then take given number (32) of random documents from each cluster and merge them into a single document;
    #because many clusters created by k-means end up as singletons, we run the k-means with  2*$PIVOTNUMBER  parameters and then use only the  $PIVOTNUMBER  largest clusters"
    #java -cp "jars/*" sparse.pivot.UnionDocuments -d ${DATAFILE} -n ${PIVOTNUMBER} -g 32 --kmeans --kmeans_centroid_file $PIVOTFILE2 --output $PIVOTFILE 2>$LOGFILE
    #echo

    prepare_run pivots-mergedrandom32-${PIVOTNUMBER} "merging randomly groups of 32 documents"
    java -cp "jars/*" sparse.pivot.UnionDocuments -c messif.objects.impl.ObjectSparseVectorCos -d ${DATAFILE} -n ${PIVOTNUMBER} -g 32 > $PIVOTFILE 2>$LOGFILE
    echo

    #echo "calculating IDF scores for all terms in the collection"
    #java -cp "jars/*" sparse.analysis.CalculateIDFs -c messif.objects.impl.ObjectSparseVectorCos -d ${DATAFILE} -l ${DATAFILESIZE} -o ${DATADIR}/idfs.bin -i ${DATADIR}/idfs.txt
    #echo

    #SOURCE_PIVOTS=${DATADIR}/pivots-mergedrandom32-${PIVOTNUMBER}.txt
    #prepare_run pivots-mergedrandom32idf1-${PIVOTNUMBER} "filter out 1% of the most frequest terms from the pivot file ($SOURCE_PIVOTS) created by merging random documents"
    #java -cp "jars/*" sparse.pivot.FilterTermsByIDF -d $SOURCE_PIVOTS -n ${PIVOTNUMBER} -i ${DATADIR}/idfs.bin -e 1 -o $PIVOTFILE 2>$LOGFILE
    #echo


    # the random projection pivots have another parameter (their dimensionality)
    #DIMENSIONALITIES="1000 2000 5000"
    DIMENSIONALITIES="2000"
    for DIM in $DIMENSIONALITIES; do

        #prepare_run pivots-randomdims${DIM}-$PIVOTNUMBER "generate our random projection pivots for pivot dimensionality $DIM"
        #./generate-pivots.py -a $MAX_TERMID -i $MIN_TERMID -d $DIM -n $PIVOTNUMBER > $PIVOTFILE 2>$LOGFILE
        #echo

        #prepare_run pivots-randomdims${DIM}-idf-${PIVOTNUMBER} "generate pivots by random projections for pivot dimensionality $DIM
        #but follow the distribution of IDF (terms with higher IDF are more likely to appear in the pivots)"
        #java -cp "jars/*" sparse.pivot.RandomDimensions -n ${PIVOTNUMBER} --dims $DIM --encourage_idf  -i ${DATADIR}/idfs.bin -o $PIVOTFILE 2>$LOGFILE
        #echo

        #prepare_run pivots-random-minlength$DIM-${PIVOTNUMBER} "running random selection for objects with given minimal length $DIM
        #WARNING: can result in less than ${PIVOTNUMBER} pivots"
        #java -cp "jars/*" sparse.pivot.ChoosePivots -p messif.pivotselection.RandomPivotChooser -d ${DATAFILE} -n ${PIVOTNUMBER} -l $DIM > $PIVOTFILE 2>$LOGFILE
        #echo
		echo
    done

done
