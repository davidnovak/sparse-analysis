package messif.objects.impl;

import gnu.trove.iterator.TIntFloatIterator;
import gnu.trove.map.TIntFloatMap;
import messif.objects.LocalAbstractObject;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.PrimitiveIterator;
import java.util.stream.IntStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author david
 */
public abstract class ObjectSparseVector extends LocalAbstractObject {

    protected final int[] positions;

    protected final float[] values;

    public ObjectSparseVector(BufferedReader stream) throws EOFException, IOException, NumberFormatException {
        // Keep reading the lines while they are comments, then read the first line of the object
        String line = readObjectComments(stream);

        String[] stringList = parseStringList(line, "[\\s:]");
        IntFloat[] pairsToSort = new IntFloat[stringList.length / 2];

        for (int i = 0; i < stringList.length; i += 2) {
            pairsToSort[i / 2] = new IntFloat(stringList[i], stringList[i + 1]);
        }
        Arrays.sort(pairsToSort, null);

        this.positions = new int[pairsToSort.length];
        this.values = new float[pairsToSort.length];
        for (int i = 0; i < pairsToSort.length; i++) {
            positions[i] = pairsToSort[i].position;
            values[i] = pairsToSort[i].value;
        }
    }

    public ObjectSparseVector(TIntFloatMap map, boolean normalize) throws EOFException, IOException, NumberFormatException {
        float normalization = normalize ? calculateMagnitude(map.values()) : 1f;
        
        IntFloat[] pairsToSort = new IntFloat[map.size()];
        int j = 0;
        for (TIntFloatIterator iterator = map.iterator(); iterator.hasNext();) {
            iterator.advance();
            pairsToSort[j ++] = new IntFloat(iterator.key(), iterator.value() / normalization);
        }
        Arrays.sort(pairsToSort, null);

        this.positions = new int[pairsToSort.length];
        this.values = new float[pairsToSort.length];
        for (int i = 0; i < pairsToSort.length; i++) {
            positions[i] = pairsToSort[i].position;
            values[i] = pairsToSort[i].value;
        }
    }

    public ObjectSparseVector(ObjectSparseVector copyFrom, boolean normalize) throws EOFException, IOException, NumberFormatException {
        
        this.positions = Arrays.copyOf(copyFrom.positions, copyFrom.positions.length);
        if (! normalize) {
            this.values = Arrays.copyOf(copyFrom.values, copyFrom.values.length);
        } else {
            float normalization = calculateMagnitude(copyFrom.values);
            this.values = new float [copyFrom.values.length];
            for (int i = 0; i < positions.length; i++) {
                values[i] = copyFrom.values[i] / normalization;
            }
        }
    }    
    
    private float calculateMagnitude(float [] values) {
        double sum = 0d;
        for (float value : values) {
            sum += value * value;
        }
        return (float) Math.sqrt(sum);        
    }
    
    protected static class IntFloat implements Comparable<IntFloat> {

        protected final int position;
        protected final float value;

        public IntFloat(String positionStr, String valueStr) {
            position = Integer.valueOf(positionStr);
            value = Float.valueOf(valueStr);
        }

        public IntFloat(int position, float value) {
            this.position = position;
            this.value = value;
        }

        @Override
        public int compareTo(IntFloat o) {
            return Integer.compare(position, o.position);
        }
    }

    @Override
    public int getSize() {
        return values.length * (Integer.BYTES + Float.BYTES) + Integer.BYTES + Integer.BYTES;
    }

    @Override
    public boolean dataEquals(Object obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int dataHashCode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        //byte [] comma = ", ".getBytes();
        for (int i = 0; i < positions.length; i++) {
            stream.write(String.valueOf(positions[i]).getBytes());
            stream.write(' ');
            stream.write(String.valueOf(values[i]).getBytes());
            if (i + 1 < positions.length) {
                stream.write(' ');
            }
        }
        stream.write('\n');
    }

    public static String[] parseStringList(String line, String separator) throws EOFException {
        if (line == null) {
            throw new EOFException();
        }
        line = line.trim();
        if (line.length() == 0) {
            return new String[0];
        }
        return line.split("\\s*" + separator + "\\s*");
    }
//    
//    public static void writeStringList(String [] data, OutputStream stream, char separator, char finalSeparator) throws IOException {
//        for (int i = 0; i < data.length; i++) {
//            if (i > 0)
//                stream.write(separator);
//            stream.write(data[i].getBytes());
//        }
//        stream.write(finalSeparator);
//    }

    public PrimitiveIterator.OfInt getPositions() {
        return Arrays.stream(positions).iterator();
    }

    public IntStream getPositionStream() {
        return Arrays.stream(positions);       
    }
    
    public int getDimensionNumber() {
        return positions.length;
    }

    /**
     *
     * @param termPosition
     * @return
     */
    public float getValue(int termPosition) {
        return values[termPosition];
    }

}
