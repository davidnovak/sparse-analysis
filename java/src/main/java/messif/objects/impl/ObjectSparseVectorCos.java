    package messif.objects.impl;

import gnu.trove.map.TIntFloatMap;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import messif.objects.LocalAbstractObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author david
 */
public class ObjectSparseVectorCos extends ObjectSparseVector {

    protected final float magnitude;
    
    public ObjectSparseVectorCos(BufferedReader stream) throws EOFException, IOException, NumberFormatException {
        super(stream);
        magnitude = calculateMagnitude();
    }

    public ObjectSparseVectorCos(TIntFloatMap map, boolean normalize) throws EOFException, IOException, NumberFormatException {
        super(map, normalize);
        magnitude = normalize ? 1f : calculateMagnitude();
    }

    public ObjectSparseVectorCos(ObjectSparseVector copyFrom, boolean normalize) throws EOFException, IOException, NumberFormatException {
        super(copyFrom, normalize);
        magnitude = normalize ? 1f : calculateMagnitude();
    }

    private float calculateMagnitude() {
        double sum = 0d;
        for (float value : values) {
            sum += value * value;
        }
        return (float) Math.sqrt(sum);        
    }

    public float getMagnitude() {
        return magnitude;
    }
    
    @Override
    protected float getDistanceImpl(LocalAbstractObject obj, float distThreshold) {
        ObjectSparseVectorCos casted = (ObjectSparseVectorCos) obj;
        int [] positions2 = casted.positions;
        float [] values2 = casted.values;
        
        // Initialize computing variables
        double dotProduct = 0;
        int i1 = 0;
        int i2 = 0;
        do {
            if (positions[i1] > positions2[i2]) {
                i2 ++;
            } else if (positions[i1] < positions2[i2]) {
                i1 ++;
            } else {
                dotProduct += values[i1 ++] * values2[i2 ++];                
            }
        } while (i1 < positions.length && i2 < positions2.length);

        double distance = 1.0 - (dotProduct / (magnitude * casted.magnitude));
        // Compensate for float sizing error
        if (distance < 0.0000001)
            return 0;
        return (float)distance;
    }
    
}
