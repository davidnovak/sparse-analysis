package sparse;

/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.AbstractObjectList;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * A common abstract
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @param <C>
 */
public abstract class DataTool<C extends LocalAbstractObject> {

    protected static final Options coreOptions;

    static {
        coreOptions = new Options();
        coreOptions.addOption(Option.builder("c").longOpt("class").hasArg().argName("class_name").desc("messif class of the data objects").build());
        coreOptions.addOption(Option.builder("d").longOpt("data_file").hasArg().argName("text_file").desc("file with sample data in text format").required().build());
    }

    public static void printUsage(String[] args, Options options, Class<?> toolClass) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(toolClass.getSimpleName(), options);
        System.err.println("\nexecuted with arguments: " + Arrays.toString(args));
    }
    
    protected Options getLineOptions() {
        return coreOptions;
    }

    protected final CommandLine lineArguments;

    protected final Class<? extends C> dataClass;
    
    protected final AbstractObjectIterator<C> dataIterator;

    private List<C> dataList;

    public DataTool(Class<? extends C> dataClass, String[] args, Options options) throws IOException, ClassNotFoundException, ParseException {
        lineArguments = new DefaultParser().parse(options, args);
        this.dataClass = Convert.getClassForName(lineArguments.getOptionValue('c', "messif.objects.impl.ObjectSparseVectorCos"), dataClass);
        // Open sample stream
        dataIterator = new StreamGenericAbstractObjectIterator<C>(
                this.dataClass, // Class of objects in file
                lineArguments.getOptionValue('d') // File name
        );
    }

    public List<C> getDataList() {
        if (dataList == null) {
            long startTime = System.currentTimeMillis();
            dataList = new AbstractObjectList<>(dataIterator);
            System.err.println("...end reading data into list, time: " + (System.currentTimeMillis() - startTime) + " ms");
        }
        return dataList;
    }

    protected List<C> readDataFile(String fileName) throws IOException, ClassNotFoundException {
        Iterator<C> iterator = new StreamGenericAbstractObjectIterator<C>(
                Convert.getClassForName(lineArguments.getOptionValue('c'), dataClass), fileName);
        return new AbstractObjectList<>(iterator);
    }
    
    /**
     * Executes the data processing task.
     * @return running time in ms
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    public abstract long processData() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException;
    
    
}
