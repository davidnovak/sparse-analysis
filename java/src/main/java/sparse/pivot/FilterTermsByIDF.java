package sparse.pivot;

/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
import gnu.trove.map.TIntFloatMap;
import gnu.trove.map.hash.TIntFloatHashMap;
import gnu.trove.set.TIntSet;
import messif.objects.impl.ObjectSparseVector;
import messif.objects.impl.ObjectSparseVectorCos;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import sparse.DataTool;
import sparse.analysis.AnalyzeOverlap;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.util.PrimitiveIterator;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class FilterTermsByIDF extends DataTool<ObjectSparseVector> {

    protected static final Options options;

    static {
        options = new Options();
        options.addOption(Option.builder("i").longOpt("idf_coefs").required().hasArg().argName("binary_file").desc("file that contains the IDF coefficients for all terms in the collection").build());
        options.addOption(Option.builder("e").longOpt("percentile").required().hasArg().argName("perc_number").desc("IDF percentile of most frequent terms to be filtered out").build());
        options.addOption(Option.builder("o").longOpt("output").hasArg().argName("text_file").desc("file to print the objects with filtered dimensions").build());
        options.addOption(Option.builder("n").longOpt("normalize").hasArg().argName("bool").desc("if true, then the output vectors are normalized").build());
        for (Option option : coreOptions.getOptions()) {
            options.addOption(option);
        }
    }

    public FilterTermsByIDF(String[] args) throws IOException, ClassNotFoundException, ParseException {
        super(ObjectSparseVectorCos.class, args, options);
    }

    
    @Override
    public long processData() throws IOException {
        long startTime = System.currentTimeMillis();
        TIntFloatMap idfMap = null;
        try (ObjectInputStream out = new ObjectInputStream(new FileInputStream(lineArguments.getOptionValue('i')))) {
            idfMap = (TIntFloatMap) out.readObject();
        } catch (ClassNotFoundException ex) {
            throw new IOException("invalid IDF file: " + lineArguments.getOptionValue('i'));
        }
        int percToFilter = Integer.valueOf(lineArguments.getOptionValue('e'));
        
        TIntSet termsToFilter = AnalyzeOverlap.getLowerIDFPercentiles(new int [] {percToFilter}, idfMap)[0];
        
        long dimensionSum = 0;
        long filteredDimSum = 0;
        double avgIdfSum = 0d;
        double filteredavgIdfSum = 0d;
        
        boolean normalize = lineArguments.hasOption('n') && Boolean.valueOf(lineArguments.getOptionValue('o'));
        int objCount = 0;
        try (PrintStream output = lineArguments.hasOption('o') ? new PrintStream(lineArguments.getOptionValue('o')) : System.out) {
            while (dataIterator.hasNext()) {
                ObjectSparseVector object = dataIterator.next();
                TIntFloatMap filteredDims = new TIntFloatHashMap(object.getDimensionNumber());

                int i = 0;
                double idfSum = 0d;
                double filteredIdfSum = 0d;
                for (PrimitiveIterator.OfInt it = object.getPositions(); it.hasNext();) {
                    int termID = it.nextInt();
                    float idf = idfMap.get(termID);
                    if (! termsToFilter.contains(termID)) {
                        filteredDims.put(termID, object.getValue(i));
                        filteredIdfSum += idf;
                    }
                    idfSum += idf;
                    i ++;
                }
                ObjectSparseVectorCos filteredObject = new ObjectSparseVectorCos(filteredDims, normalize);
                filteredObject.write(output);
                
                dimensionSum += object.getDimensionNumber();
                filteredDimSum += filteredObject.getDimensionNumber();
                avgIdfSum += (idfSum / object.getDimensionNumber());
                double result = filteredIdfSum / filteredObject.getDimensionNumber();
                filteredavgIdfSum += (result == Double.NaN) ? 0d : result;
                objCount ++;
            }
        }
        
        System.err.println("avg # of non-zero dimensions: " + ((double) dimensionSum / objCount));
        System.err.println("avg # of non-zero dimensions (after filtering): " + ((double) filteredDimSum / objCount));
        System.err.println("avg value of IDF: " + ((double) avgIdfSum / objCount));
        System.err.println("avg value of IDF (after filtering): " + ((double) filteredavgIdfSum / objCount));

        return System.currentTimeMillis() - startTime;
    }

    
    /**
     * Main method that deals with the parameters and computes the histogram.
     *
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            FilterTermsByIDF filterByIDF = new FilterTermsByIDF(args);
            // Begin computation
            long runningTime = filterByIDF.processData();
            System.err.println("running time: " + (runningTime / 1000f) + " s");

        } catch (IOException | ClassNotFoundException | ParseException e) {
            e.printStackTrace();
            printUsage(args, options, FilterTermsByIDF.class);
        }
    }
}
