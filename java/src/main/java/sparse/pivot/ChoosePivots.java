package sparse.pivot;

/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */

import messif.buckets.FilterRejectException;
import messif.buckets.OccupationLowException;
import messif.objects.LocalAbstractObject;
import messif.objects.ObjectProvider;
import messif.objects.impl.ObjectSparseVector;
import messif.objects.impl.ObjectSparseVectorCos;
import messif.pivotselection.AbstractPivotChooser;
import messif.utility.Convert;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import sparse.DataTool;

import java.io.IOException;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ChoosePivots extends DataTool<ObjectSparseVector> {

    protected static final Options options;

    static {
        options = new Options();
        options.addOption(Option.builder("n").longOpt("number").required().hasArg().argName("number").desc("number of pivots to select").required().build());
        options.addOption(Option.builder("p").longOpt("pivot_chooser_class").hasArg().argName("class_name").desc("messif class of pivot chooser (default: KMeans)").build());
        options.addOption(Option.builder("l").longOpt("min_doc_length").hasArg().argName("length").desc("if used, only documents with given lenght are considered").build());
        for (Option option : coreOptions.getOptions()) {
            options.addOption(option);
        }
    }

    public ChoosePivots(String[] args) throws IOException, ClassNotFoundException, ParseException {
        super(ObjectSparseVectorCos.class, args, options);
    }

    
    @Override
    public long processData() throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        long startTime = System.currentTimeMillis();
        int numberPivots = Integer.valueOf(lineArguments.getOptionValue('n'));
        // create the pivot chooser here
        Class<AbstractPivotChooser> classForName = Convert.getClassForName(lineArguments.getOptionValue('p', "messif.pivotselection.KMeansPivotChooser"), AbstractPivotChooser.class);
        AbstractPivotChooser pivotChooser = classForName.newInstance();

        ObjectProvider<? extends LocalAbstractObject> dataToUse;
        if (lineArguments.hasOption('l')) {
            try {
                final int minDocLength = Integer.valueOf(lineArguments.getOptionValue('l'));
                dataToUse = this.dataIterator.getMatchingObjects(objectSparseVector -> objectSparseVector.getSize() >= minDocLength ? 0 : 1);
            } catch (OccupationLowException | FilterRejectException e) {
                e.printStackTrace();
                return -1l;
            }
        } else {
            dataToUse = this.dataIterator;
        }

        // Begin computation
        pivotChooser.registerSampleProvider(dataToUse);
        pivotChooser.selectPivot(numberPivots);

        for (int i = 0; i < numberPivots; i++) {
            pivotChooser.getPivot(i).write(System.out);
        }
        return System.currentTimeMillis() - startTime;
    }
    
    /**
     * Main method that deals with the parameters and computes the histogram.
     *
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            ChoosePivots pivotChooser = new ChoosePivots(args);
            // Begin computation
            long runningTime = pivotChooser.processData();
            System.err.println("running time: " + (runningTime / 1000f) + " s");

        } catch (Exception e) {
            e.printStackTrace();
            printUsage(args, options, ChoosePivots.class);
        }
    }
}
