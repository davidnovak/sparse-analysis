package sparse.pivot;

/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
import messif.objects.LocalAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.pivotselection.AbstractPivotChooser;
import messif.utility.Convert;

import java.io.PrintStream;

/**
 * Allows to compute a distance histogram of a given metric space.
 * The metric space is represented by the data object class, the distances
 * are measured by taking random object pairs from a given data file.
 *
 * <p>
 * The whole data is read in and then random pairs are identified in the data.
 * </p>
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ChoosePivotsOld {

    /**
     * Prints the this class usage from the command line.
     * @param stream a stream to which the usage info is written
     */
    private static void printUsage(PrintStream stream) {
        stream.println("Usage: ChoosePivotsOld <class> <data file> <number-of-pivots> [<pivot-chooser-class>]");
        return;
    }

    /**
     * Main method that deals with the parameters and computes the histogram.
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            if (args.length < 3) {
                printUsage(System.err);                
                return;
            }
            
            // Open sample stream
            StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(
                    Convert.getClassForName(args[0], LocalAbstractObject.class), // Class of objects in file
                    args[1] // File name
            );
            final int numberPivots = Integer.valueOf(args[2]);
            
            // create the pivot chooser here
            String pivotChooserStr = args.length > 3 ? args[3] : "messif.pivotselection.KMeansPivotChooser";
            Class<AbstractPivotChooser> classForName = Convert.getClassForName(pivotChooserStr, AbstractPivotChooser.class);
            AbstractPivotChooser pivotChooser = classForName.newInstance();

            // Begin computation
            long time = System.currentTimeMillis();
            pivotChooser.registerSampleProvider(iterator);
            pivotChooser.selectPivot(numberPivots);
            System.err.println("pivot selection time: " + ((System.currentTimeMillis() - time) /1000) + " s");
            
            for (int i = 0; i < numberPivots; i++) {
                pivotChooser.getPivot(i).write(System.out);
            }
            
        } catch (Throwable e) {
            e.printStackTrace();
            printUsage(System.err);
        }
    }
}
