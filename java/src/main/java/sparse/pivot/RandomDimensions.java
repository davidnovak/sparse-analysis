package sparse.pivot;

import gnu.trove.map.TIntFloatMap;
import gnu.trove.map.hash.TIntFloatHashMap;
import messif.objects.impl.ObjectSparseVectorCos;
import org.apache.commons.cli.*;
import sparse.DataTool;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.PrimitiveIterator;
import java.util.Random;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RandomDimensions {

    protected static final Options options;

    static {
        options = new Options();
        options.addOption(Option.builder("n").longOpt("number").required().hasArg().argName("number").desc("number of random clusters to union").build());
        options.addOption(Option.builder("m").longOpt("dims").required().hasArg().argName("number").desc("number of dimensions to randomly choose").build());
        options.addOption(Option.builder("o").longOpt("output").hasArg().argName("text_file").desc("file to print the generated pivots to").build());
        options.addOption(Option.builder("i").longOpt("idf_coefs").required().hasArg().argName("binary_file").desc("file that contains the IDF coefficients for all terms in the collection").build());
        options.addOption(Option.builder("f").longOpt("flat").desc("generate normalized vectors with all (non-zero) dimensions equal to the same value").build());
        options.addOption(Option.builder("e").longOpt("encourage_idf").desc("pick dimension i with probability equal to its IDF").build());
    }

    protected final CommandLine lineArguments;

    private final Random random = new Random(System.currentTimeMillis());

    public RandomDimensions(String[] args) throws IOException, ClassNotFoundException, ParseException {
        lineArguments = new DefaultParser().parse(options, args);
    }

    public long processData() throws IOException {
        TIntFloatMap idfMap = null;
        try (ObjectInputStream out = new ObjectInputStream(new FileInputStream(lineArguments.getOptionValue('i')))) {
            idfMap = (TIntFloatMap) out.readObject();
        } catch (ClassNotFoundException ex) {
            throw new IOException("invalid IDF file: " + lineArguments.getOptionValue('i'));
        }
        
        int[] termIDs = idfMap.keys();
        int randomDims = Integer.valueOf(lineArguments.getOptionValue('m'));
        int number = Integer.valueOf(lineArguments.getOptionValue('n'));
        boolean flat = lineArguments.hasOption('f');

        // transform the term-IDF map to make probability intervals space
        float [] idfIntervals = null;
        if (lineArguments.hasOption('e')) {
            idfIntervals= new float [termIDs.length];
            idfIntervals[0] = idfMap.get(termIDs[0]);
            for (int i = 1; i < termIDs.length; i++) {
                idfIntervals[i] = idfIntervals[i - 1] + idfMap.get(termIDs[i]);
            }
        }
        
        long startTime = System.currentTimeMillis();
        try (PrintStream output = lineArguments.hasOption('o') ? new PrintStream(lineArguments.getOptionValue('o')) : System.out) {
            for (int j = 0; j < number; j++) {
                TIntFloatMap randomMap = idfIntervals == null ?
                        getRandomDims(termIDs, randomDims, idfMap, flat) :
                        getRandomDimsIDF(termIDs, idfIntervals, randomDims, idfMap, flat);
                
                ObjectSparseVectorCos unionedObject = new ObjectSparseVectorCos(randomMap, true);
                unionedObject.write(output);
            }
        }
            
            // merge selected objects
//            IntStream mergedStream = dataList.get(randomNumbers[0]).getPositionStream();
//            for (int i = 1; i < number; i++) {
//                mergedStream = IntStream.concat(mergedStream, dataList.get(randomNumbers[i]).getPositionStream());
//            }
//            int [] unionedPositions = mergedStream.distinct().toArray();
//            Arrays.sort(unionedPositions);            
            // create values for these positions (dimensions)
        return System.currentTimeMillis() - startTime;
    }
    
    protected TIntFloatMap getRandomDims(int[] termIDs, int randomDims, TIntFloatMap idfMap, boolean flat) {
        // select "n" objects at random
        int[] randomNumbers = random.ints(0, termIDs.length).distinct().limit(randomDims).toArray();

        TIntFloatMap randomMap = new TIntFloatHashMap();
        for (int i = 0; i < randomDims; i++) {
            randomMap.put(termIDs[randomNumbers[i]], 
                    flat ? 1f : idfMap.get(termIDs[randomNumbers[i]]));
        }
        String toString = Arrays.toString(randomNumbers);
        System.err.println("selected dimensions: " + toString.substring(0, Math.min(200, toString.length())) + "...");
        return randomMap;
    }

    protected TIntFloatMap getRandomDimsIDF(int[] termIDs, float [] idfIntervals, int randomDims, TIntFloatMap idfMap, boolean flat) {
        // select "n" objects at random
        PrimitiveIterator.OfDouble doubles = random.doubles(0d, idfIntervals[idfIntervals.length - 1]).iterator();
        
//        System.err.println("idfMap: " + idfMap.toString());
//        System.err.println("termIDs: " + Arrays.toString(termIDs));
//        System.err.println("idfIntervals: " + Arrays.toString(idfIntervals));
        
        TIntFloatMap randomMap = new TIntFloatHashMap();
        for (int i = 0; i < randomDims; i++) {
            int index = 0;
            do {
                float randomFloat = (float) doubles.nextDouble();
                index = Arrays.binarySearch(idfIntervals, 0, idfIntervals.length, randomFloat);
                if (index < 0) {
                    index = (- index) - 1;
                }
                //System.err.println("selected termID " + termIDs[index] + " with index " + index + " because of random value " + randomFloat);
            } while (randomMap.containsKey(termIDs[index]));
            randomMap.put(termIDs[index], 
                    flat ? 1f : idfMap.get(termIDs[index]));
        }
        String toString = Arrays.toString(randomMap.keys());
        System.err.println("selected dimensions: " + toString.substring(0, Math.min(200, toString.length())) + "...");
        return randomMap;
    }
    
    /**
     * Main method that deals with the parameters and computes the histogram.
     *
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            RandomDimensions randomDimProjector = new RandomDimensions(args);
            // Begin computation
            long runningTime = randomDimProjector.processData();
            System.err.println("running time: " + (runningTime / 1000f) + " s");

        } catch (IOException | ClassNotFoundException | ParseException e) {
            e.printStackTrace();
            DataTool.printUsage(args, options, RandomDimensions.class);
        }
    }
}
