package sparse.pivot;

import gnu.trove.map.TIntFloatMap;
import gnu.trove.map.hash.TIntFloatHashMap;
import messif.objects.LocalAbstractObject;
import messif.objects.impl.ObjectSparseVector;
import messif.objects.impl.ObjectSparseVectorCos;
import messif.objects.util.AbstractObjectList;
import messif.pivotselection.KMeansPivotChooser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import sparse.DataTool;

import java.io.IOException;
import java.io.PrintStream;
import java.util.*;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class UnionDocuments extends DataTool<ObjectSparseVector> {

    protected static final Options options;

    Random random = new Random(System.currentTimeMillis());
    
    static {
        options = new Options();
        options.addOption(Option.builder("n").longOpt("number").required().hasArg().argName("number").desc("number of random clusters to union").build());
        options.addOption(Option.builder("g").longOpt("groupsize").required().hasArg().argName("number").desc("sizes of random clusters to union").build());
        options.addOption(Option.builder("o").longOpt("output").hasArg().argName("text_file").desc("file to print the unioned objects to").build());
        options.addOption(Option.builder("k").longOpt("kmeans").desc("the docs to be unioned are selected not at random but as k-means clusters").build());
        options.addOption(Option.builder("m").longOpt("kmeans_centroid_file").hasArg().argName("text_file").desc("file to print the unioned objects to").build());
        for (Option option : coreOptions.getOptions()) {
            options.addOption(option);
        }
    }

    public UnionDocuments(String[] args) throws IOException, ClassNotFoundException, ParseException {
        super(ObjectSparseVector.class, args, options);
    }

    @Override
    public long processData() throws IOException {
        int groupSize = Integer.valueOf(lineArguments.getOptionValue('g'));
        int number = Integer.valueOf(lineArguments.getOptionValue('n'));
                
        long startTime = System.currentTimeMillis();
        try (PrintStream output = lineArguments.hasOption('o') ? new PrintStream(lineArguments.getOptionValue('o')) : System.out) {
            List<List> listsToSelectFrom;
            if (lineArguments.hasOption('k')) {
                listsToSelectFrom = runKMeans(number, lineArguments.hasOption('m') ? lineArguments.getOptionValue('m') : null);
            } else {
                List dataList = getDataList();
                listsToSelectFrom = new ArrayList(number);
                for (int i = 0; i < number; i++) {
                    listsToSelectFrom.add(dataList);
                }
//                int nObjects = dataList.size();
//
//                if (nObjects < groupSize) {
//                    throw new IllegalArgumentException("at least " + groupSize +" data objects necessary");
//                }
            }
            for (int j = 0; j < number; j++) {
                TIntFloatMap unionedMap = unionRandom(listsToSelectFrom.get(j), groupSize);
                
                // construct the merged document normalizing the values
                ObjectSparseVectorCos unionedObject = new ObjectSparseVectorCos(unionedMap, true);
                unionedObject.write(output);
            }
        }
            
            // merge selected objects
//            IntStream mergedStream = dataList.get(randomNumbers[0]).getPositionStream();
//            for (int i = 1; i < number; i++) {
//                mergedStream = IntStream.concat(mergedStream, dataList.get(randomNumbers[i]).getPositionStream());
//            }
//            int [] unionedPositions = mergedStream.distinct().toArray();
//            Arrays.sort(unionedPositions);            
            // create values for these positions (dimensions)
        return System.currentTimeMillis() - startTime;
    }

    protected TIntFloatMap unionRandom(List list, int groupSize) {
        if (list.size() < groupSize) {
            System.err.println("object list has less then " + groupSize + " objects: " + list.size());
            groupSize = list.size();
        }
        // select "n" objects at random
        int[] randomNumbers = random.ints(0, list.size()).distinct().limit(groupSize).toArray();

        TIntFloatMap unionedMap = new TIntFloatHashMap();
        for (int i = 0; i < groupSize; i++) {
            ObjectSparseVector randomObj = (ObjectSparseVector) list.get(randomNumbers[i]);
            int k = 0;
            for (PrimitiveIterator.OfInt iterator = randomObj.getPositions(); iterator.hasNext();) {
                int dimension = iterator.nextInt();
                unionedMap.putIfAbsent(dimension, randomObj.getValue(k ++));
            }
        }
        System.err.println("merged the objects number: " + Arrays.toString(randomNumbers));
        return unionedMap;
    }
    
    protected final static float KMEANS_COEF = 2f;
    
    protected List<List> runKMeans(int number, String centroidFileName) {
        KMeansPivotChooser kMeansPivotChooser = new KMeansPivotChooser();
        kMeansPivotChooser.registerSampleProvider(dataIterator);
        kMeansPivotChooser.selectPivot((int) (number * KMEANS_COEF));

        List<PivotClusterPair> allClusters = new ArrayList<>((int) (number * KMEANS_COEF));
        int i = 0;
        for (AbstractObjectList<LocalAbstractObject> cluster : kMeansPivotChooser.getClusters()) {
            allClusters.add(new PivotClusterPair(kMeansPivotChooser.getPivot(i), cluster));
            i++;
        }
        Collections.sort(allClusters);
        
        List<List> retVal = new ArrayList<>(number);
        for (i = 0; i < number; i++) {
            retVal.add(allClusters.get(i).cluster);
        }
        // print also kMeans centroids (pivots)
        if (centroidFileName != null) {
            try (PrintStream output = new PrintStream(centroidFileName)) {
                for (i = 0; i < number; i++) {
                    allClusters.get(i).pivot.write(output);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return retVal;
    }

    static class PivotClusterPair implements Comparable<PivotClusterPair> {
        final LocalAbstractObject pivot;
        final AbstractObjectList<LocalAbstractObject> cluster;

        PivotClusterPair(LocalAbstractObject pivot, AbstractObjectList<LocalAbstractObject> cluster) {
            this.pivot = pivot;
            this.cluster = cluster;
        }

        @Override
        public int compareTo(PivotClusterPair o) {
            return Integer.compare(o.cluster.size(), cluster.size());
        }
    }
    
    /**
     * Main method that deals with the parameters and computes the histogram.
     *
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            UnionDocuments idfCalculator = new UnionDocuments(args);
            // Begin computation
            long runningTime = idfCalculator.processData();
            System.err.println("running time: " + (runningTime / 1000f) + " s");

        } catch (IOException | ClassNotFoundException | ParseException e) {
            e.printStackTrace();
            printUsage(args, options, UnionDocuments.class);
        }
    }
}
