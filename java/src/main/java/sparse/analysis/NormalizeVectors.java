package sparse.analysis;

import java.io.IOException;
import java.io.PrintStream;
import messif.objects.impl.ObjectSparseVector;
import messif.objects.impl.ObjectSparseVectorCos;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import sparse.DataTool;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class NormalizeVectors extends DataTool<ObjectSparseVector> {

    protected static final Options options;

    static {
        options = new Options();
        options.addOption(Option.builder("o").longOpt("output").hasArg().argName("text_file").desc("file to print the objects with filtered dimensions").build());
        for (Option option : coreOptions.getOptions()) {
            options.addOption(option);
        }
    }

    public NormalizeVectors(String[] args) throws IOException, ClassNotFoundException, ParseException {
        super(ObjectSparseVector.class, args, options);
    }

    @Override
    public long processData() throws IOException {
        long startTime = System.currentTimeMillis();
        try (PrintStream output = lineArguments.hasOption('o') ? new PrintStream(lineArguments.getOptionValue('o')) : System.out) {
            while (dataIterator.hasNext()) {
                new ObjectSparseVectorCos(dataIterator.next(), true).write(output);
            }
        }
        return System.currentTimeMillis() - startTime;
    }

    /**
     * Main method that deals with the parameters and computes the histogram.
     *
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            NormalizeVectors normalizer = new NormalizeVectors(args);
            // Begin computation
            long runningTime = normalizer.processData();
            System.err.println("running time: " + (runningTime / 1000f) + " s");

        } catch (IOException | ClassNotFoundException | ParseException e) {
            e.printStackTrace();
            printUsage(args, options, NormalizeVectors.class);
        }
    }
}
