package sparse.analysis;

import gnu.trove.iterator.TIntIntIterator;
import gnu.trove.map.TIntFloatMap;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.hash.TIntFloatHashMap;
import gnu.trove.map.hash.TIntIntHashMap;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import messif.objects.impl.ObjectSparseVector;
import messif.objects.impl.ObjectSparseVectorCos;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import sparse.DataTool;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class CalculateIDFs extends DataTool<ObjectSparseVector> {

    protected static final Options options;

    static {
        options = new Options();
        options.addOption(Option.builder("l").longOpt("length").hasArg().argName("number").desc("number of data objects").build());
        options.addOption(Option.builder("o").longOpt("idf_map_output").hasArg().argName("file_name").desc("file to serialize the IDF map").build());
        options.addOption(Option.builder("i").longOpt("idf_text_output").hasArg().argName("file_name").desc("file to print the text IDF to").build());
        for (Option option : coreOptions.getOptions()) {
            options.addOption(option);
        }
    }

    public CalculateIDFs(String[] args) throws IOException, ClassNotFoundException, ParseException {
        super(ObjectSparseVectorCos.class, args, options);
    }

    @Override
    public long processData() throws IOException {
        int docCount = 0;
        TIntIntMap dfMap = lineArguments.hasOption('l')
                ? new TIntIntHashMap(Integer.valueOf(lineArguments.getOptionValue('l'))) : new TIntIntHashMap();
        long startTime = System.currentTimeMillis();
        while (dataIterator.hasNext()) {
            ObjectSparseVector object = dataIterator.next();
            object.getPositions().forEachRemaining((int dimension) -> {
                dfMap.adjustOrPutValue(dimension, 1, 1);
            });
            docCount++;
        }

        TIntFloatMap idfMap = new TIntFloatHashMap(dfMap.size());
        try (PrintStream output = lineArguments.hasOption('i') ? new PrintStream(lineArguments.getOptionValue('i')) : System.out) {
            output.println("termid\ttf\tlog10(" + docCount + "/tf)");
            double docCountFl = (double) docCount;
            for (TIntIntIterator iterator = dfMap.iterator(); iterator.hasNext();) {
                iterator.advance();
                float idf = (float) Math.log10(docCountFl / iterator.value());
                idfMap.put(iterator.key(), idf);

                output.print(iterator.key());
                output.print("\t");
                output.print(iterator.value());
                output.print("\t");
                output.println(idf);
            }
        }
        
        // now serialize the IDF map, if specified
        if (lineArguments.hasOption('o')) {
            try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(lineArguments.getOptionValue('o')))) {
                out.writeObject(idfMap);
            }
        }
        System.err.println("processed " + docCount + " documents with " + dfMap.size() + " unique terms");
        
        return System.currentTimeMillis() - startTime;
    }

    /**
     * Main method that deals with the parameters and computes the histogram.
     *
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            CalculateIDFs idfCalculator = new CalculateIDFs(args);
            // Begin computation
            long runningTime = idfCalculator.processData();
            System.err.println("running time: " + (runningTime / 1000f) + " s");

        } catch (IOException | ClassNotFoundException | ParseException e) {
            e.printStackTrace();
            printUsage(args, options, CalculateIDFs.class);
        }
    }
}
