package sparse.analysis;

/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */

import gnu.trove.set.TIntSet;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.impl.ObjectSparseVectorCos;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.util.PrimitiveIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

//import mindex.MIndexPP;
//import mindex.MIndexProperties;
//import mindex.MIndexProperty;
//import mindex.MIndexStdInitializer;
//import mindex.MetricIndexes;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class AnalyzePivots extends AnalyzeOverlap {

    protected static final Options pivotOptions;

    static {
        pivotOptions = new Options();
        pivotOptions.addOption(Option.builder("r").longOpt("random_pairs").hasArg().argName("number").desc("number of random pairs to check").build());
        pivotOptions.addOption(Option.builder("i").longOpt("idf_coefs").hasArg().argName("binary_file").desc("file that contains the IDF coefficients for all terms in the collection").build());
        
        pivotOptions.addOption(Option.builder("p").longOpt("pivot_file").required().hasArg().argName("text_file").desc("file with pivots to analyse").build());
        pivotOptions.addOption(Option.builder("o").longOpt("random_objects").required().hasArg().argName("number").desc("number of random objects from the dataset set to be tested").build());
        pivotOptions.addOption(Option.builder("f").longOpt("prefix_lengths").hasArg().argName("length_list").desc("list of permutation prefixes to consider; e.g.: -f 1,4,8,16,32").build());
        for (Option option : coreOptions.getOptions()) {
            pivotOptions.addOption(option);
        }
    }

    public AnalyzePivots(String[] args) throws IOException, ClassNotFoundException, ParseException {
        super(ObjectSparseVectorCos.class, args, pivotOptions);
    }

    @Override
    public long processData() throws IOException, ClassNotFoundException, InstantiationException {
        try {
            long retVal = 0;
            if (lineArguments.hasOption('r')) {
                int randomPairs = Integer.valueOf(lineArguments.getOptionValue('r'));
                retVal += analyzeOverlapRandom(randomPairs);
            }
            retVal += analyzePivotSet(Integer.valueOf(lineArguments.getOptionValue('o')));
            
            return retVal;
        } catch (AlgorithmMethodException ex) {
            Logger.getLogger(AnalyzePivots.class.getName()).log(Level.SEVERE, null, ex);
            throw new InstantiationException();
        }
    }

    /**
     * Analyzes the quality of the pivot set.
     * @param randomObjects
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws AlgorithmMethodException 
     */
    protected long analyzePivotSet(int randomObjects) throws IOException, ClassNotFoundException, InstantiationException, AlgorithmMethodException {
//        List<ObjectSparseVectorCos> dataList = getDataList();
//        List<ObjectSparseVectorCos> pivotList = readDataFile(lineArguments.getOptionValue('p'));
//        int nObjects = dataList.size();
//        int nPivots = pivotList.size();
//        if (nObjects < 1 || (nPivots < 1)) {
//            throw new IllegalArgumentException("at least two data objects necessary");
//        }
//
//        // initialize all counters usually sums to be divided by the size of the sample set in the end
//        // sum of numbers of overallping dimenstions
//        long overlappingSum = 0L;
//        // sum of ratios of the ratios of magnitude of the overlapping dimensions
//        double relOveralMagnitudeSum = 0d;
//        long dimSum1 = 0L;
//        long dimSum2 = 0L;
//        long maxPivotDimSum = 0L;
//        long maxOverlappingSum = 0L;
//        double maxRelOverlapMagSum = 0d;
//        double overallRelOverlMagnSum = 0d;
//
//        // these settings are needed if several prefixes of permutation are to be analyzed
//        int [] prefixes = null;
//        MIndexStdInitializer mIndex = null;
//        double [] magnitudeRatioSumPrefs = null;
//        if (lineArguments.hasOption('f')) {
//            String [] prefStrings = lineArguments.getOptionValue('f').split(",");
//            prefixes = Stream.of(prefStrings).mapToInt((pref) -> Integer.valueOf(pref)).toArray();
//            magnitudeRatioSumPrefs = new double [prefStrings.length];
//            MIndexProperties mIndexProps = new MIndexProperties(new Properties(), "");
//            mIndexProps.setProperty(MIndexProperty.PIVOT_FILE, lineArguments.getOptionValue('p'));
//            mIndexProps.setProperty(MIndexProperty.DATA_CLASS, lineArguments.getOptionValue('c'));
//            mIndexProps.setProperty(MIndexProperty.NUMBER_OF_PIVOTS, (short) pivotList.size());
//            mIndexProps.setProperty(MIndexProperty.MIN_LEVEL, (short) 1);
//            mIndexProps.setProperty(MIndexProperty.MAX_LEVEL, (short) IntStream.of(prefixes).max().getAsInt());
////            mIndexProps.setProperty(MIndexProperty.MAX_LEVEL, (short) pivotList.size());
//            mIndex = new MIndexStdInitializer(mIndexProps);
//        }
//
//        long startTime = System.currentTimeMillis();
//        // iterate over given number of randomly selected objects from the dataset
//        for (int randomCounter = 0; randomCounter < randomObjects; randomCounter++) {
//            ObjectSparseVectorCos object = dataList.get(random.nextInt(nObjects));
//            int maxOverlap = -1;
//            double maxRelOverlapMagnitude = -1d;
//            ObjectSparseVectorCos maxPivot = null;
//            TIntSet allOverlappingPositions = new TIntHashSet();
//
//            // initialize the pivot prefixes for given object
//            List<TShortSet> permutationPrefixes = new ArrayList<>();
//            List<TIntSet> allOverPosForPrefs = new ArrayList<>();
//            if (mIndex != null) {
//                mIndex.initPP(object);
//                MIndexPP permutation = MetricIndexes.readMIndexPP(object);
//                for (int prefix : prefixes) {
//                    permutationPrefixes.add(new TShortHashSet(Arrays.copyOf(permutation.getPPPForReading(), prefix)));
//                    allOverPosForPrefs.add(new TIntHashSet());
//                }
//            }
//
//            short pivotNumber = 0;
//            // iterate over all pivots in the pivot set
//            for (ObjectSparseVectorCos pivot : pivotList) {
//                OverllapsAndMagnitudes result = findOverlap(object.getPositions(), pivot.getPositions(), object, null);
//                if (result.relOverlMag > maxRelOverlapMagnitude) {
//                    maxPivot = pivot;
//                    maxOverlap = result.overlap;
//                    maxRelOverlapMagnitude = result.relOverlMag;
//                }
//                overlappingSum += result.overlap;
//                relOveralMagnitudeSum += result.relOverlMag;
//                dimSum2 += pivot.getDimensionNumber();
//
//                allOverlappingPositions.addAll(result.overlappingPositions);
//
//                if (mIndex != null) {
//                    int j = 0;
//                    for (TShortSet permutationPrefix : permutationPrefixes) {
//                        if (permutationPrefix.contains(pivotNumber)) {
//                            allOverPosForPrefs.get(j).addAll(result.overlappingPositions);
//                        }
//                        j ++;
//                    }
//                }
//                pivotNumber ++;
//            }
//            dimSum1 += object.getDimensionNumber();
//
//            maxOverlappingSum += maxOverlap;
//            maxRelOverlapMagSum += maxRelOverlapMagnitude;
//            maxPivotDimSum += maxPivot.getDimensionNumber();
//
//            overallRelOverlMagnSum += getRelativeOverlapMagnitude(allOverlappingPositions, object);
//
//            if (mIndex != null) {
//                for (int j = 0; j < magnitudeRatioSumPrefs.length; j++) {
//                    magnitudeRatioSumPrefs [j] += getRelativeOverlapMagnitude(allOverPosForPrefs.get(j), object);
//                }
//            }
//
//        }
//        System.out.println("average number of non-zero dimentions in the set of data objects: " + ((double) dimSum1 / (randomObjects)));
//        System.out.println("average number of non-zero dimentions in the set of pivots: " + ((double) dimSum2 / (randomObjects * pivotList.size())));
//        printResult(randomObjects * pivotList.size(), overlappingSum, relOveralMagnitudeSum, -1);
//
//        System.out.println("\n values between a random object and pivot that MAXIMIZES the relative overal magnitude:\n");
//        System.out.println("\taverage number of non-zero dimentions of the maximizing pivot: " + ((double) maxPivotDimSum / (randomObjects)));
//        printResult(randomObjects, maxOverlappingSum, maxRelOverlapMagSum, -1);
//        //System.out.println("Average minimal distance: " + ((double) distanceSum / (randomPairs)));
//
//        System.out.println("\naverage overall relative object-pivot overlap magnitude: " + (overallRelOverlMagnSum / (randomObjects)));
//        if (mIndex != null) {
//            for (int j = 0; j < magnitudeRatioSumPrefs.length; j++) {
//                System.out.println("average overall relative object-pivot overlap magnitude in prefix of length: " + prefixes[j] + ": " + magnitudeRatioSumPrefs [j] / (randomObjects));
//            }
//        }

        long startTime = System.currentTimeMillis();
        return System.currentTimeMillis() - startTime;
    }

    
    protected double getRelativeOverlapMagnitude(TIntSet overlappingDims, ObjectSparseVectorCos object) {
        double overlappingSum = 0d;
        PrimitiveIterator.OfInt positions = object.getPositions();
        int i = 0;
        while (positions.hasNext()) {
            if (overlappingDims.contains(positions.nextInt())) {
                overlappingSum += object.getValue(i) * object.getValue(i);
            }
            i ++;
        }
        return Math.sqrt(overlappingSum) / object.getMagnitude();
    }
    
    protected void printResult(int randomPairs, long overlappingSum, double magnitudeRatioSum, int percentile) {
        if (percentile > 0) {
            System.out.println("When ignoring " + percentile + " percentile of most frequesnt terms: ");
        }
        System.out.println("\taverage number of overlapping non-zero dimensions: " + ((double) overlappingSum / randomPairs));
        System.out.println("\taverage relative (left-hand) vector overlap magnitude: " + (magnitudeRatioSum / randomPairs));        
        //System.out.println("\taverage relative (left-hand) vector overlap magnitude: " + (magnitudeRatioSum / randomPairs));        
    }
    
    /**
     * Main method that deals with the parameters and computes the histogram.
     *
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            AnalyzePivots pivotAnalyser = new AnalyzePivots(args);
            // Begin computation
            long runningTime = pivotAnalyser.processData();
            System.err.println("running time: " + (runningTime / 1000f) + " s");

        } catch (IOException | ClassNotFoundException | ParseException | InstantiationException e) {
            e.printStackTrace();
            printUsage(args, pivotOptions, AnalyzePivots.class);
        }
    }
}
