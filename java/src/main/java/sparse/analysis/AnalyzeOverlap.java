package sparse.analysis;

/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
import gnu.trove.iterator.TIntFloatIterator;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntFloatMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import messif.objects.impl.ObjectSparseVectorCos;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import sparse.DataTool;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.PrimitiveIterator;
import java.util.Random;
import java.util.stream.DoubleStream;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class AnalyzeOverlap extends DataTool<ObjectSparseVectorCos> {

    protected static final Options options;

    static {
        options = new Options();
        options.addOption(Option.builder("r").longOpt("random_pairs").required().hasArg().argName("number").desc("number of random pairs to check").build());
        options.addOption(Option.builder("i").longOpt("idf_coefs").hasArg().argName("binary_file").desc("file that contains the IDF coefficients for all terms in the collection").build());
        for (Option option : coreOptions.getOptions()) {
            options.addOption(option);
        }
    }

    public AnalyzeOverlap(String[] args) throws IOException, ClassNotFoundException, ParseException {
        super(ObjectSparseVectorCos.class, args, options);
//            // create the pivot chooser here
//            String pivotChooserStr = args.length > 3 ? args[3] : "messif.pivotselection.KMeansPivotChooser";
//            Class<AbstractPivotChooser> classForName = Convert.getClassForName(pivotChooserStr, AbstractPivotChooser.class);
//            AbstractPivotChooser pivotChooser = classForName.newInstance();            
    }

    protected AnalyzeOverlap(Class<? extends ObjectSparseVectorCos> dataClass, String[] args, Options options) throws IOException, ClassNotFoundException, ParseException {
        super(dataClass, args, options);
    }
    
    @Override
    public long processData() throws IOException, ClassNotFoundException, InstantiationException {
        long retVal = 0;
        int randomPairs = Integer.valueOf(lineArguments.getOptionValue('r'));
        retVal += analyzeOverlapRandom(randomPairs);

        return retVal;
    }

    protected final Random random = new Random(System.currentTimeMillis());
    
    protected long analyzeOverlapRandom(int randomPairs) throws IOException, ClassNotFoundException {
        TIntFloatMap idfMap = null;
        if (lineArguments.hasOption('i')) {
            try (ObjectInputStream out = new ObjectInputStream(new FileInputStream(lineArguments.getOptionValue('i')))) {
                idfMap = (TIntFloatMap) out.readObject();
            } catch (ClassNotFoundException ex) {
                throw new IOException("invalid IDF file: " + lineArguments.getOptionValue('i'));
            }
        }
        List<ObjectSparseVectorCos> dataList = getDataList();
        int nObjects = dataList.size();
        if (nObjects < 2) {
            throw new IllegalArgumentException("at least two data objects necessary");
        }
        
        // find n random pairs
        long overlappingSum = 0L;
        //double distanceSum = 0d;
        double magnitudeRatioSum = 0d;
        
        long startTime = System.currentTimeMillis();
        double dimSum = 0L;
        boolean idfFilters = idfMap != null && lineArguments.hasOption('e');
        int [] percentiles = null;
        String[] percStrings = null;
        if (idfFilters) {
            percStrings = lineArguments.getOptionValue('e').split(",");
            percentiles = new int [percStrings.length];
            for (int i = 0; i < percStrings.length; i++) {
                percentiles[i] = Integer.valueOf(percStrings[i]);
            }
        }
        final long [] idfFilterOverlapSums = idfFilters ? new long[percStrings.length] : null;
        final double [] idfFilterRatioSums = idfFilters ? new double[percStrings.length] : null;
        TIntSet [] lowerIDFPercentiles = idfFilters? getLowerIDFPercentiles(percentiles, idfMap) : null;

        for (int pairsCounter = 1; pairsCounter <= randomPairs; pairsCounter++) {
            int random1 = random.nextInt(nObjects);
            int random2;
            do {
                random2 = random.nextInt(nObjects);
            } while (random1 == random2);

            ObjectSparseVectorCos object1 = dataList.get(random1);
            ObjectSparseVectorCos object2 = dataList.get(random2);
            PrimitiveIterator.OfInt positions1 = object1.getPositions();
            PrimitiveIterator.OfInt positions2 = object2.getPositions();

            // find the overlapping non-zero dimensions
            final OverllapsAndMagnitudes result = findOverlap(positions1, positions2, object1, lowerIDFPercentiles);
            //float distance = object1.getDistance(object2);
            //distanceSum += distance;

            overlappingSum += result.overlap;
            magnitudeRatioSum += result.relOverlMag;
            dimSum += object1.getDimensionNumber() + object2.getDimensionNumber();
//                dimSum2 += object2.getDimensionNumber();
            if (idfFilters) {
                Arrays.setAll(idfFilterOverlapSums, i -> idfFilterOverlapSums[i] + result.idfFilterOverlaps[i]);
                Arrays.setAll(idfFilterRatioSums, i -> idfFilterRatioSums[i] + result.idfFilterRatios[i]);
            }
        }

        System.out.println("Average number of non-zero dimentions in the objects: " + ((double) dimSum / (randomPairs * 2)));
        //System.out.println("Average distance: " + ((double) distanceSum / (randomPairs)));
        printResult(randomPairs, overlappingSum, magnitudeRatioSum, -1);
        if (idfFilters) {
            for (int i = 0; i < percentiles.length; i++) {
                printResult(randomPairs, idfFilterOverlapSums[i], idfFilterRatioSums[i], percentiles[i]);                
            }
        }

        return System.currentTimeMillis() - startTime;
    }

    protected OverllapsAndMagnitudes findOverlap(PrimitiveIterator.OfInt positions1, PrimitiveIterator.OfInt positions2, final ObjectSparseVectorCos object1, TIntSet[] lowerIDFPercentiles) {
        int overlapping = 0;
        double overlappingMagnitude = 0d;
        int [] idfFilterOverlaps = null;
        double[] idfFilterWeights = null;
        if (lowerIDFPercentiles != null) {
            idfFilterOverlaps = new int [lowerIDFPercentiles.length];
            idfFilterWeights = new double [lowerIDFPercentiles.length];
        }
        
        int pos1 = positions1.nextInt();
        int pos2 = positions2.nextInt();
        TIntList overlappingPositions = new TIntArrayList();
        int dimCounter1 = 0;
        while (pos1 > 0 && pos2 > 0) {
            if (pos1 > pos2) {
                pos2 = positions2.hasNext() ? positions2.nextInt() : -1;
            } else if (pos1 < pos2) {
                pos1 = positions1.hasNext() ? positions1.nextInt() : -1;
                dimCounter1 ++;
            } else {
                overlapping++;
                overlappingPositions.add(pos1);
                float dimValue = object1.getValue(dimCounter1);
                overlappingMagnitude += dimValue * dimValue;
                if (lowerIDFPercentiles != null) {
                    for (int i = 0; i < lowerIDFPercentiles.length; i++) {
                        if (!lowerIDFPercentiles[i].contains(pos1)) {
                            idfFilterOverlaps[i]++;
                            idfFilterWeights[i] += dimValue * dimValue;
                        }
                    }
                }
                pos1 = positions1.hasNext() ? positions1.nextInt() : -1;
                pos2 = positions2.hasNext() ? positions2.nextInt() : -1;
                dimCounter1 ++;
            }
        }
        double[] idfFilterRatios = null;
        if (lowerIDFPercentiles != null) {
            idfFilterRatios = DoubleStream.of(idfFilterWeights).map((double d) -> Math.sqrt(d) / object1.getMagnitude() ).toArray();
        }
        return new OverllapsAndMagnitudes(overlapping, overlappingPositions, Math.sqrt(overlappingMagnitude) / object1.getMagnitude(), idfFilterOverlaps, idfFilterRatios);
    }
    
    protected static class OverllapsAndMagnitudes {
        protected final int overlap;
        protected final double relOverlMag;
        protected final TIntList overlappingPositions;
        protected final int [] idfFilterOverlaps;
        protected final double[] idfFilterRatios;

        public OverllapsAndMagnitudes(int overlap, TIntList overlappingPositions, double ratio, int[] idfFilterOverlaps, double[] idfFilterRatios) {
            this.overlap = overlap;
            this.relOverlMag = ratio;
            this.overlappingPositions = overlappingPositions;
            this.idfFilterOverlaps = idfFilterOverlaps;
            this.idfFilterRatios = idfFilterRatios;
        }
        
    }

//    protected void updateSumsForPercentiles(int position, long[] percOverlappingSums, TIntSet [] lowerIDFPercentiles) { // float idf,  double[] percOverlappingIDFSums, 
//        for (int i = 0; i < lowerIDFPercentiles.length; i++) {
//            if (! lowerIDFPercentiles[i].contains(position)) {
//                percOverlappingSums[i] ++;
////                percOverlappingIDFSums[i] += idf;
//            }            
//        }
//    }
    
    public static TIntSet [] getLowerIDFPercentiles(int [] percentiles, final TIntFloatMap idfMap) {
        TermIDF [] termIDs = new TermIDF[idfMap.size()];
        int i = 0;        
        for (TIntFloatIterator iterator = idfMap.iterator(); iterator.hasNext();) {
            iterator.advance();
            termIDs[i ++] = new TermIDF(iterator.key(), iterator.value());
        }
        Arrays.sort(termIDs, null);
        
        TIntSet [] lowerIDFPercentiles = new TIntSet[percentiles.length];
        i = 0;
        for (int perc : percentiles) {
            TIntHashSet topTermIDs = new TIntHashSet();            
            // find out the number of records belonging to the percentile
            int numberOfTerms = (int) ((perc / 100f) * idfMap.size());
            for (int j = 0; j < numberOfTerms; j++) {
                topTermIDs.add(termIDs[j].termID);
            }
            lowerIDFPercentiles[i ++] = topTermIDs;            
        }
        
        return lowerIDFPercentiles;
    }
    
    protected void printResult(int randomPairs, long overlappingSum, double magnitudeRatioSum, int percentile) {
        if (percentile > 0) {
            System.out.println("When ignoring " + percentile + " percentile of most frequesnt terms: ");
        }
        System.out.println("\taverage number of overlapping non-zero dimensions: " + ((double) overlappingSum / randomPairs));
        System.out.println("\taverage ratio of the (left hand) vector magnitude in the overlap: " + (magnitudeRatioSum / randomPairs));        
    }
    
    
    
    protected static class TermIDF implements Comparable<TermIDF> {

        protected final int termID;
        protected final float idf;

        public TermIDF(int termID, float idf) {
            this.termID = termID;
            this.idf = idf;
        }
        
        @Override
        public int compareTo(TermIDF o) {
            return Float.compare(idf, o.idf);
        }
        
    }
    
    /**
     * Main method that deals with the parameters and computes the histogram.
     *
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            AnalyzeOverlap analyseOverlap = new AnalyzeOverlap(args);
            // Begin computation
            long runningTime = analyseOverlap.processData();
            System.err.println("running time: " + (runningTime / 1000f) + " s");

        } catch (IOException | ClassNotFoundException | ParseException | InstantiationException e) {
            e.printStackTrace();
            printUsage(args, options, AnalyzeOverlap.class);
        }
    }
}
